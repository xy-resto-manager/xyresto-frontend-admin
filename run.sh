#!/bin/bash

docker stop xyresto-frontend-admin
docker rm xyresto-frontend-admin
docker run --name xyresto-frontend-admin \
           -p 81:80 \
		   --mount type=bind,source=$(pwd)/nginx.conf,target=/etc/nginx/nginx.conf \
		   --mount type=bind,source=$(pwd)/public,target=/var/www/html \
		   -d \
		   nginx:latest 