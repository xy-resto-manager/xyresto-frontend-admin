$.ajax({
    type: 'GET',
    url: '/backend/api/user/current-user',
    success: () => { window.location.href = '/' },
    error: (e) => {
        if (e.responseJSON.errors.find(error => error.error === 'UsernameNotFoundException') === undefined) {
            console.log(e);
        }
    }
});

function onContentLoad() {
    if (window.location.search.includes("reg=1")) {
        $('.loggedin').fadeIn(150);
    }

    $('#login').click(tryLogin);
    $(document).keypress((e) => {
        if (e.which == 13) tryLogin();
    });
}

function tryLogin() {
    if (!$('.login-form')[0].checkValidity()) {
        $('<input type="submit">').hide().appendTo($('.login-form')).click().remove();
        return;
    }
    let postData = {
        email: $('#email').val(),
        password: $('#password').val()
    }
    $.ajax({
        type: 'POST',
        url: '/backend/api/user/_login',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: loginSuccess,
        error: loginFailed
    });
}

function loginSuccess(response) {
    if (response.data.roles.includes('ADMIN')) {
        window.location.href = '/';
    } else {
        $.ajax({
            type: 'POST',
            url: '/backend/api/user/_logout',
            dataType: 'json',
            success: loginFailed,
            error: (e) => {
                console.log(e);
            }
        });
    }
}

function loginFailed(e) {
    $('.error').fadeIn(150);
}