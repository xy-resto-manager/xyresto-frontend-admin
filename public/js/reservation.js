let page = 1;
let currentStatus = localStorage.getItem('currentStatus') ? localStorage.getItem('currentStatus') : "PENDING"
let itemsPerPage;
let totalItems;
function onContentLoad(){
    getData()
    setActive(currentStatus)
    $('#status-pending').click(() =>{
        page = 1;
        currentStatus = "PENDING"
        setActive("pending")
        getData()
    })
    $('#status-approved').click(() =>{
        page = 1;
        currentStatus = "APPROVED"
        setActive("approved")
        getData()
    })
    $('#status-declined').click( () =>{
        page = 1;
        currentStatus = "DECLINED"
        setActive("declined")
        getData()
    })
    
    $('.new-reservation-btn').click(()=>{
        window.location.href="/new-reservation.html"
    })
 

    $('.reservation-data-list').scroll(() => {
       
        if($('.reservation-data-list').scrollTop() + $('.reservation-data-list').height() + 100 >  $('.reservation-data-list').prop('scrollHeight')){     
            if(page * itemsPerPage < totalItems){
                page++
                getMoreData()
            }
        }
    })
    
}

function setActive(status){
    status = status.toLowerCase()
    $('.status-label').removeClass('status-active')
    $('#status-'+status).addClass('status-active')
    $(".reservation-detail").css({"transform": "translateX(101%)","opactiy":0})
    localStorage.setItem('currentStatus',status.toUpperCase())
     
}

function getData(){
    console.log(currentStatus)
    $.ajax({
        type: 'GET',
        url: "/backend/api/admin/reservation/reservations?page="+page +"&status="+currentStatus,
        success: (resp) => {
            loadReservationData(resp)
        },
        error: (e) => {
            console.log(e)
        }
    });

}

function loadReservationData(response){
    totalItems = response.pagination.totalItems;
    itemsPerPage = response.pagination.itemsPerPage;
    $("#reservation-data-item-template").hide()
    $(".reservation-data-item:not(#reservation-data-item-template)").remove()
    let template = $("#reservation-data-item-template")
    let listData = $(".reservation-data-list")
    response.data.forEach( Element => {

        let data = template.clone().removeAttr("id");
        data.show()
        data.find(".reservation-item-id").html(Element.id)
        data.find(".date").html(getDate(Element.timestamp))
        data.find(".time").html(getTime(Element.timestamp))
        data.find(".table-number-no").html(Element.tableId)
        data.find('.reservation-item-detail-btn > a').click(()=>{
            showReservationDetail(Element.id)
        })
        listData.append(data)
    });
    $('.spinner').hide()
    $('.reservation-data-list').show()
}

function getMoreData(){
    console.log()
    $.ajax({
        type: 'GET',
        url: "/backend/api/admin/reservation/reservations?page="+page +"&status="+currentStatus,
        success: (resp) => {
            loadMoreReservationData(resp)
        },
        error: (e) => {
            console.log(e)
        }
    });
}

function loadMoreReservationData(response){
    let template = $("#reservation-data-item-template")
    let listData = $(".reservation-data-list")
    response.data.forEach( Element => {
        let data = template.clone().removeAttr("id");
        data.show()
        data.find(".reservation-item-id").html(Element.id)
        data.find(".date").html(getDate(Element.timestamp))
        data.find(".time").html(getTime(Element.timestamp))
        data.find(".table-number-no").html(Element.tableId)
        data.find('.reservation-item-detail-btn > a').click(()=>{
            showReservationDetail(Element.id)
        })
        listData.append(data)
    });
}

//Show Reservation Detail (Overlay)

function showReservationDetail(id){
    console.log(id)
    $.ajax({
        type: 'GET',
        url: "/backend/api/reservation/reservations/"+id,
        success: (resp) => {
            
            $(".reservation-item-id").html(resp.data.id)
            $(".date").html(getDate(resp.data.timestamp))
            $(".time").html(getTime(resp.data.timestamp))
            $(".table-number-no").html(resp.data.tableId)
            $("#order-item-template").hide()
            $(".order-item:not(#order-item-template)").remove()
            
            let orderlist = $(".orders-list")
            let totalPrice = 0;

            resp.data.orders.forEach(orderedProduct => {

                let template = $("#order-item-template").clone()
                template.show().removeAttr('id')
                template.find("#item-quantity").html(orderedProduct.quantity+"x")
                template.find("#item-name").html(orderedProduct.product.name)

                if (orderedProduct.product.imageUrl != null) {
                    $.ajax({
                        type: 'GET',
                        url: '/backend' + orderedProduct.product.imageUrl,
                        xhr: () => {
                            let xhr = new XMLHttpRequest();
                            xhr.responseType = 'blob';
                            return xhr;
                        },
                        success: (response) => {
                            template.find('img').attr('src', URL.createObjectURL(response));
                        },
                        error: function (e) {
                            console.log(e);
                        }
                    });
                }

                orderlist.append(template)
                totalPrice += orderedProduct.quantity * orderedProduct.product.price
            });

            $(".fullName").html(resp.data.fullName)
            $('.user-email').html(resp.data.email)
            $('.order-total > p > span').html(totalPrice)
        },
        error: (e) => {
            console.log(e)
        }
    });

    $(".reservation-detail").css({"transform": "translateX(0%)"})
    $('.reservation-detail').find(".btn-group").show()

    if(currentStatus == "PENDING"){
        $('.order-detail').css("justify-content","space-between")
        $('.approve-btn').show()
        $('.cancel-btn').show()
        $('.dining-btn').hide()

        $('.approve-btn').click(()=>{
            editReservationStatus(id,"APPROVED")
        })
        $('.cancel-btn').click(()=>{
            editReservationStatus(id,"DECLINED")
        })
    }else if(currentStatus == "APPROVED"){
        $('.approve-btn').hide()
        $('.cancel-btn').hide()
        $('.dining-btn').show()

        $('.dining-btn').click(()=>{
            editReservationStatus(id,"DINING")
        })
    }else{
        $('.reservation-detail').find(".btn-group").hide()
    }
}

function editReservationStatus(id,status){
    let postData = {
        status : status
    }
    $.ajax({
        type: "PUT",
        url: "/backend/api/admin/reservation/reservations/"+id,
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: "json",
        success: function (response) {
            returnToList()
        },
        error : function (error){
            console.log(error)
        }
    });
}

function returnToList(){
    $(".reservation-detail").css({"transform": "translateX(101%)"})
    getData()
}


function getTime(timestamp){
    let time = new Date(timestamp)
    let hour = time.getHours().toString();
    let nextHour = (time.getHours() + 1).toString();
    return hour + ":00 - " + nextHour + ":00"
}

function getDate(timestamp){
    let time = new Date(timestamp)
    let year = time.getFullYear().toString();
    let monthName = getMonthName(time.getMonth());
    let date = time.getDate().toString();
    let dayName = getDayName(time.getDay())

    return dayName + date + monthName + year
}

function getMonthName(month){
    switch (month) {
        case 0:
            return " January ";
        case 1:
            return " February ";
        case 2:
            return " March ";
        case 3:
            return " April ";
        case 4:
            return " May ";
        case 5:
            return " June ";
        case 6:
            return " July ";
        case 7:
            return " August ";
        case 8:
            return " September ";
        case 9:
            return " October ";
        case 10:
            return " November ";
        case 11:
            return " December ";
    }
}

function getDayName(day){
    switch (day) {
        case 0:
            return " Sunday, "
        case 1:
            return " Monday, "
        case 2:
            return " Tuesday, "
        case 3:
            return " Wednesday, "
        case 4:
            return " Thursday, "
        case 5:
            return " Friday, "
        case 6:
            return " Saturday, "
    }
}

