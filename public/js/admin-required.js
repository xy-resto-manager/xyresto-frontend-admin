$.ajax({
    type: 'GET',
    url: '/backend/api/user/current-user',
    success: (response) => {
        if (!response.data.roles.includes('ADMIN')) {
            $.ajax({
                type: 'POST',
                url: '/backend/api/user/_logout',
                dataType: 'json',
                success: () => {
                    window.location.href = '/login'
                },
                error: (e) => {
                    console.log(e);
                }
            });
        }
    },
    error: (e) => {
        window.location.href = '/login';
    }
});