let modalDisplayed = false;
let userList = null;

function onContentLoad() {
    fetchAdmins();

    $('.modal-add-admin').click(() => {
        hideModal();
    })

    $('.modal-add-admin .modal').click((e) => {
        e.stopPropagation();
    });

    $('.admin-save').click(() => {
        let warningNotFound = $('.modal-warning-notfound');
        let warningAlreadyAdmin = $('.modal-warning-alreadyadmin');
        let email = $('#email').val();
        let users = userList.filter(user => user.email === email);
        if (users.length === 0) {
            warningAlreadyAdmin.slideUp(() => {
                warningNotFound.slideDown();
            });
            return;
        }
        if (users.length === 1 && users[0].roles.includes('ADMIN')) {
            warningNotFound.slideUp(() => {
                warningAlreadyAdmin.slideDown();
            });
            return;
        }
        let id = userList.filter(user => user.email === email)[0].id;
        let postData = {
            roles: ['USER', 'ADMIN']
        }
        $.ajax({
            method: 'POST',
            url: `/backend/api/admin/user/users/${id}/roles`,
            data: JSON.stringify(postData),
            dataType: 'json',
            contentType: 'application/json',
            success: (resp) => {
                hideModal();
                fetchAdmins();
            },
            error: (e) => {
                console.log(e);
            }
        })
    });

    $('.admin-cancel').click(() => {
        hideModal();
    });

    $(document).keypress((e) => {
        if (modalDisplayed && e.key === 'Enter') {
            $('.admin-save').click();
        }
    });

    $('.add-admin').click(() => {
        $.ajax({
            method: 'GET',
            url: '/backend/api/admin/user/users',
            dataType: 'json',
            contentType: 'application/json',
            success: (resp) => {
                userList = resp.data;
                showModal();
            },
            error: (e) => {
                console.log(e);
            }
        })
    });
}

function showModal() {
    $('.modal-add-admin').fadeIn(150);
    modalDisplayed = true;
}

function hideModal() {
    $('.modal-add-admin').fadeOut(150);
    $('.modal-warning-notfound').slideUp();
    $('.modal-warning-alreadyadmin').slideUp();
    modalDisplayed = false;
}

function fetchAdmins() {
    $.ajax({
        method: 'GET',
        url: '/backend/api/admin/user/users?role=ADMIN',
        dataType: 'json',
        contentType: 'application/json',
        success: (resp) => {
            loadAdmins(resp);
        },
        error: (e) => {
            console.log(e);
        }
    })
}

function loadAdmins(resp) {
    let template = $('#admin-template');
    $('.admin:not(#admin-template)').remove();
    let admins = $('.admins');
    resp.data.forEach((data) => {
        let admin = template.clone().removeAttr('id');
        admin.find('h2').html(data.fullName);
        admin.find('span').html(data.email);
        admin.find('.admin-delete').click(() => {
            deleteAdmin(admin, data.id);
        });
        admins.append(admin);
    });
}

function deleteAdmin(element, id) {
    let postData = {
        roles: ['USER']
    };
    $.ajax({
        method: 'POST',
        url: `/backend/api/admin/user/users/${id}/roles`,
        dataType: 'json',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        success: (resp) => {
            element.remove();
        },
        error: (e) => {
            console.log(e);
        }
    });
}