let canvas = null;
let ctx = null;
let width = null;
let height = null;
let gridWidth = null;
let gridHeight = null;
let tileWidth = null;
let tileHeight = null;
let nextTableId = 0;
let selectedTable = -1;
let selectedTableX = null;
let selectedTableY = null;
let dragging = false;
let tableLayout = new Array(5).fill(0).map(() => new Array(5).fill(0));
let booked = new Array(5).fill(0).map(() => new Array(5).fill(0));

//SelectedTables = start from 0,0
// xPos,yPos = start from 1,1

function recalculateSizes() {
    canvas = document.querySelector('#canvas');
    ctx = canvas.getContext('2d');
    width = Math.round(canvas.offsetWidth);
    height = Math.round(canvas.offsetHeight);
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    gridWidth = width / 5;
    gridHeight = height / 5;
    tileWidth = gridWidth * 0.8;
    tileHeight = gridHeight * 0.8;
}

function onContentLoad(){
    recalculateSizes()

    $(window).resize(() => {
        $('#canvas').removeAttr('width').removeAttr('height');
        recalculateSizes();
        drawLayout();
    });

    getLayout()
    $('.new-table').click(addTable)
    $('#canvas').mousedown((e) => {selectTable(e)})
    $('.delete-btn').click(deleteTable)

    $('#canvas').mousemove((e) => {dragTable(e)})
    $('#canvas').mouseup(()=>{dragging = false})
    $('#canvas').mouseleave(()=>{dragging = false})

    $('.approve-btn').click(()=>sendData())
    $('.ok-btn').click(() =>  {
        $('.overlay').fadeOut(150)
        })


    // Button Hover styling
    $('.approve-btn').hover(
        ()=>{ // Hover in
            $('.approve-btn').css({'background-color':'#038600',"color":'white'})
        },
        ()=>{
            $('.approve-btn').css({'background-color':'white',"color":'#038600'})
        }
    )

    $('.delete-btn').hover(
        ()=>{ // Hover in
            $('.delete-btn').css({'background-color':'#C30000',"color":'white'})
        },
        ()=>{
            $('.delete-btn').css({'background-color':'white',"color":'#C30000'})
        }
    )

    $('.ok-btn').hover(
        ()=>{ // Hover in
            $('.ok-btn').css({'background-color':'#E4C102',"color":'white'})
        },
        ()=>{
            $('.ok-btn').css({'background-color':'white',"color":'#E4C102'})
        }
    )

}

function getLayout(){

    $.ajax({
        type: 'GET',
        url: '/backend/api/admin/layout/tables',
        dataType : 'json',
        success : initTableLayout,
        error: function(e){
            console.log(e)
    }
    });
}
function initTableLayout(response){
    response.data.forEach(table => {
        var x = table.table.x;
        var y = table.table.y;
        var tableId = table.table.tableId;
        tableLayout[y-1][x-1] = tableId;
        booked[y-1][x-1] = table.booked ? 1 : 0

        // nextTableId = (tableId > nextTableId) ? tableId : nextTableId // DELETE THIS
    });
    nextTableId=getNextTableId(); // THIS ONE
    $('.new-table > div > p').html(nextTableId)

    // console.log(tableLayout)
    // console.log(booked)
    // console.log(nextTableId)
    drawLayout()
}

function drawLayout(){
    ctx.beginPath()
    ctx.clearRect(0,0,width,height)
    tableLayout.forEach((row,rowIndex) => {
        row.forEach((tableId,colIndex) => {
            if(tableId != 0){
                // console.log(tableId)
                if(tableLayout[rowIndex][colIndex] == selectedTable){
                    drawSelected(rowIndex,colIndex,tableId)
                }else{
                    drawNotSelected(rowIndex,colIndex,tableId)
                }
            }
        })
    });
    // console.log(tableLayout)
}

function drawNotSelected(row,col,tableId){

    ctx.beginPath()
    let startXDrawPos = (col) * gridWidth + 0.1 * gridWidth;
    let startYDrawPos = (row) * gridHeight + 0.1 * gridHeight;
    ctx.strokeStyle = "#E4C102"
    ctx.clearRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight)
    ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#242424"
    ctx.font = "20px Georgia"
    ctx.fillText(tableId,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.45 * gridHeight))
}

function addTable(){
    // console.log(tableLayout)
    if(nextTableId > 25){
        console.log("REALLY BRO? ")
    }
    let found = false;
    for(var i = 0; i < 5 ;i++){
        for(var j = 0; j < 5;j++){
            if(tableLayout[i][j] == 0){
                tableLayout[i][j] = nextTableId;
                nextTableId=getNextTableId(); // THIS ONE
                // console.log(nextTableId)
                found = true;
                break;
            }
        }
        if(found) break;
    }
    $('.new-table > div > p').html(nextTableId)
    drawLayout()
}

function selectTable(e){
    dragging = true;
    // console.log(tableLayout)
    let xPos = Math.ceil(e.offsetX / gridWidth)
    let yPos = Math.ceil(e.offsetY / gridHeight)

    tableLayout.forEach((row,rowIndex)=>{
        row.forEach((tableId,colIndex)=>{
            if(isTable(rowIndex,colIndex,yPos,xPos) && tableId != 0){
                if(selectedTable != -1){
                    drawNotSelected(selectedTableY,selectedTableX,selectedTable)
                }
                drawSelected(rowIndex,colIndex,tableId)
                selectedTable = tableId
                selectedTableX = colIndex
                selectedTableY = rowIndex
                $('.table-details-table-number > span').html(tableId)
            }
        })
    })

}

function drawSelected(row,col,tableId){

    ctx.beginPath();
    let startXDrawPos = (col) * gridWidth + 0.1 * gridWidth;
    let startYDrawPos = (row) * gridHeight + 0.1 * gridHeight;
    ctx.fillStyle = "#E4C102"
    ctx.fillRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#242424"
    ctx.font = "20px Georgia"
    ctx.fillText(tableId,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.45 * gridHeight))

}


function deleteTable(){
    if(selectedTable != -1 && tableLayout[selectedTableY][selectedTableX] != 0 && booked[selectedTableY][selectedTableX] == 0){

     for(var i = 0; i < 5;i++){
        for(var j = 0; j < 5;j++){
            if(tableLayout[i][j] == selectedTable) tableLayout[i][j] = 0;
            }
        }
        // console.log(tableLayout)
        selectedTable = -1;
        selectedTableX = -1;
        selectedTableY = -1;
        nextTableId=getNextTableId(); // THIS ONE
        // console.log(nextTableId)
        drawLayout()
        $('.new-table > div > p').html(nextTableId)
        $('.table-details-table-number > span').html("-")
    }else{
        $('.overlay').css({opacity:0,display:"flex"}).animate({
            opacity: 1},
        150)
    }
}

function dragTable(e){
    let xPos = Math.ceil(e.offsetX / gridWidth) - 1
    let yPos = Math.ceil(e.offsetY / gridHeight) - 1
            // Dragging current Table
            if(dragging == true && selectedTable != -1){
                //Detect Changes in Position && Check if Collision
                if((yPos != selectedTableY || xPos != selectedTableX) && tableLayout[yPos][xPos] == 0){
                    tableLayout[selectedTableY][selectedTableX] = 0;
                    tableLayout[yPos][xPos] = selectedTable
                    selectedTableX = xPos;
                    selectedTableY = yPos;
                    drawLayout()
                }

            }


}

function getNextTableId(){
    var nextTable = 1;
    var found = false;
    for(var i = 0; i < 5; i++){
        for (var j = 0; j < 5;j++){
            if(tableLayout[i][j] === nextTable){
                nextTable+=1;
                i = -1;
                j = 0;
                break;
            }
        }
    }
    return nextTable;
}

function isTable(rowIndex,colIndex,yPos,xPos){
    return (rowIndex == yPos - 1 && colIndex == xPos - 1)
}


// API
function sendData(){

    let putData = []
    tableLayout.forEach((row,rowIndex)=>{
        row.forEach((tableId,colIndex)=> {
            if(tableLayout[rowIndex][colIndex] != 0){
                    let xPos = colIndex + 1;
                    let yPos = rowIndex + 1;
                    let table = {
                        tableId : tableId,
                        x : xPos,
                        y : yPos
                    }
                    putData.push(table)
            }
        })
    })
    console.log(JSON.stringify(putData))
    $.ajax({
        type: 'PUT',
        url: '/backend/api/admin/layout/tables',
        data: JSON.stringify(putData),
        contentType: 'application/json',
        dataType: 'json',
        success: function (response) {
            window.location.href = "/layout"
        },error: function(e){
            console.log(e)
        }
    });


}
