let canvas = null;
let ctx = null;
let width = null;
let height = null;
let gridWidth = null;
let gridHeight = null;
let tileWidth = null;
let tileHeight = null;

function onContentLoad(){

    canvas = document.querySelector('#canvas');
    ctx = canvas.getContext('2d');
    width = Math.round(canvas.offsetWidth);
    height = Math.round(canvas.offsetHeight);
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    gridWidth = width / 5;
    gridHeight = height / 5;
    tileWidth = gridWidth * 0.8;
    tileHeight = gridHeight * 0.8;

    getLayout()


}

function getLayout(){

    $.ajax({
        type: 'GET',
        url: '/backend/api/admin/layout/tables',
        dataType : 'json',
        success : drawLayout,
        error: function(e){
            console.log(e)
    }
    });
}

function drawLayout(response){
    console.log(response.data)
    ctx.clearRect(0,0,this.width,this.height)
    response.data.forEach(table => {
        ctx.beginPath();
        let startXDrawPos = (table.table.x - 1) * gridWidth + 0.1 * gridWidth;
        let startYDrawPos = (table.table.y - 1) * gridHeight + 0.1 * gridHeight;
        ctx.strokeStyle = "#E4C102"
        ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
        ctx.fillStyle = "#242424"
        ctx.font = "20px Georgia"
        ctx.fillText(table.table.tableId,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight))
    
    });

}