const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

let page = 1;

function onContentLoad() {
    fetchOrders();

    $('.orders').scroll(function() {
        let elem = $(this)[0];
        if (elem.scrollHeight - elem.offsetHeight - elem.scrollTop <= 1) {
            page++;
            fetchOrders();
        }
    });

    $('.overlay').click(() => {
        $('.order-detail').fadeOut(150);
    });


}

function fetchOrders() {
    $.ajax({
        method: 'GET',
        url: `/backend/api/admin/reservation/reservations?status=DINING&page=${page}`,
        dataType: 'json',
        contentType: 'application/json',
        success: (resp) => {
            loadOrders(resp);
        },
        error: (e) => {
            console.log(e);
        }
    });
}

function viewDetail(data) {
    let detail = $('.order-detail');

    let date = new Date(data.timestamp);
    let dateAfter = new Date(data.timestamp);
    dateAfter.setTime(dateAfter.getTime() + 2 * 60 * 60 * 1000);
    detail.find('.order-detail-date').html(`${DAYS[date.getDay()]}, ${date.getDate()} ${MONTHS[date.getMonth()]} ${date.getFullYear()}`);
    detail.find('.order-detail-time').html(`${String(date.getHours()).padStart(2, '0')}:${String(dateAfter.getMinutes()).padStart(2, '0')} - ${String(dateAfter.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`);

    detail.find('.order-detail-tablenum').html(data.tableId);

    detail.find('.reserver-name').html(data.fullName);
    detail.find('.reserver-email').html(data.email);

    let totalprice = 0;

    let template = $('#orderedproduct-template');
    let orderedproducts = detail.find('.modal-content');
    $('.orderedproduct:not(#orderedproduct-template)').remove();
    data.orders.forEach((orderdata) => {
        let orderedproduct = template.clone().removeAttr('id');
        orderedproduct.find('.orderedproduct-amount').html(orderdata.quantity);
        orderedproduct.find('.orderedproduct-name').html(orderdata.product.name);
        totalprice += orderdata.quantity * orderdata.product.price;

        if (!orderdata.served) {
            orderedproduct.find('.orderedproduct-done').hide();
            orderedproduct.find('.orderedproduct-setdone').click(() => {
                let payload = {
                    productIds: [orderdata.product.id]
                };
                $.ajax({
                    method: 'PUT',
                    url: `/backend/api/admin/reservation/reservations/${data.id}/orders`,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(payload),
                    success: (resp) => {
                        orderedproduct.find('.orderedproduct-setdone').remove();
                        orderedproduct.find('.orderedproduct-done').show();
                    },
                    error: (e) => {
                        console.log(e);
                    }
                })
            });
        } else {
            orderedproduct.find('.orderedproduct-setdone').remove();
        }

        if (orderdata.product.imageUrl != null) {
            $.ajax({
                type: 'GET',
                url: '/backend' + orderdata.product.imageUrl,
                xhr: () => {
                    let xhr = new XMLHttpRequest();
                    xhr.responseType = 'blob';
                    return xhr;
                },
                success: (response) => {
                    orderedproduct.find('.orderedproduct-image').attr('src', URL.createObjectURL(response));
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }

        orderedproducts.append(orderedproduct);
    });
    detail.find('.reservation-total').html('Rp. ' + totalprice);

    $('.finished-btn').css({"pointer-events":"auto","background-color":"#E4C102","color":"white","border" : "2px solid #E4C102"})
    $('.finished-btn').click(()=>{
        let postData = {
            status : "FINISHED"
        }
        $.ajax({
            type: "PUT",
            url: "/backend/api/admin/reservation/reservations/"+data.id,
            data: JSON.stringify(postData),
            contentType: 'application/json',
            dataType: "json",
            success: function (response) {
                $('.finished-btn').css({"pointer-events":"none","background-color":"#F9F7F0","color":"#c4c2c2","border" : "2px solid #c4c2c2"})

                fetchOrders()
            },
            error : function (error){
                console.log(error)
            }
        });
    })

    detail.fadeIn(150);
}

function loadOrders(resp) {
    let template = $('#order-template');
    let orders = $('.orders');

    if (resp.data.length === 0) {
        $('.orders').off('scroll');
    }

    resp.data.forEach((data) => {
        let order = template.clone().removeAttr('id');
        order.find('.order-table-number').html(data.tableId);
        let date = new Date(data.timestamp);
        let dateAfter = new Date(data.timestamp);
        dateAfter.setTime(dateAfter.getTime() + 2 * 60 * 60 * 1000);
        order.find('.order-date').html(`${DAYS[date.getDay()]}, ${date.getDate()} ${MONTHS[date.getMonth()]} ${date.getFullYear()}`);
        order.find('.order-time').html(`${String(date.getHours()).padStart(2, '0')}:${String(dateAfter.getMinutes()).padStart(2, '0')} - ${String(dateAfter.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`);

        order.find('.order-info').click(() => {
            viewDetail(data);
        });
        orders.append(order);
    });
    $('.spinner').hide()
}
