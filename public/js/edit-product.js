var product_id = window.location.href.split('/');
if (window.location.href.includes('create-product')) product_id = null;
else product_id = parseInt(product_id[product_id.length - 1]);

var curfile = null;

function onContentLoad() {
    if (product_id) {
        $.ajax({
            method: 'GET',
            url: '/backend/api/product/' + product_id,
            contentType: 'application/json',
            success: (resp) => {
                setContent(resp.data);
                $('.overlay').fadeOut(250);
            },
            error: (e) => {
                console.log(e);
            }
        });
    } else {
        loadCategories(null);
        $('.product-image').attr('src', '/img/placeholder.png');
        $('.overlay').fadeOut(250);
    }

    $('.product-image').on('drop', (e) => {
        e.preventDefault();
        if (e.originalEvent.dataTransfer.items && e.originalEvent.dataTransfer.items.length === 1) {
            if (e.originalEvent.dataTransfer.items[0].kind === 'file') {
                let file = e.originalEvent.dataTransfer.items[0].getAsFile();
                setImage(file);
            }
        } else if (e.originalEvent.dataTransfer.files.length === 1) {
            let file = e.originalEvent.dataTransfer.files[0];
            setImage(file);
        }
    }).on('dragover', (e) => {
        e.preventDefault();
    }).click(() => {
        $('<input>').attr('type', 'file').change(function() {
            let file = $(this)[0].files[0];
            setImage(file);
        }).click();
    });

    $('.save').click(() => {
        let traits = []
        if ($('#trait-savory').prop('checked')) traits.push('SAVORY');
        if ($('#trait-sweet').prop('checked')) traits.push('SWEET');
        if ($('#trait-spicy').prop('checked')) traits.push('SPICY');
        let postData = {
            name: $('#name').val(),
            description: $('#description').val(),
            price: parseInt($('#price').val()),
            traits: traits,
            recommended: $('#recommended').prop('checked')
        };
        let cat = parseInt($('#category').val());
        if (cat !== -1) postData['category'] = cat;

        $.ajax({
            method: product_id ? 'PUT' : 'POST',
            url: product_id ? ('/backend/api/admin/product/' + product_id) : '/backend/api/admin/product/products',
            data: JSON.stringify(postData),
            dataTransfer: 'json',
            contentType: 'application/json',
            success: (resp) => {
                if (curfile != null) {
                    let formdata = new FormData();
                    curfile.arrayBuffer().then((data) => {
                        formdata.append('file', curfile);
                        $.ajax({
                            method: 'POST',
                            url: '/backend/api/admin/product/' + resp.data.id + '/image',
                            dataType: 'json',
                            data: formdata,
                            contentType: false,
                            processData: false,
                            success: () => {
                                if (product_id) window.location.href = '/products?e=1';
                                else window.location.href = '/products?c=1';
                            },
                            error: (e) => {
                                console.log(e);
                            }
                        });
                    });

                } else {
                    if (product_id) window.location.href = '/products?e=1';
                    else window.location.href = '/products?c=1';
                }
            },
            error: (e) => {
                console.log(e);
            }
        });
    });
}

function setImage(file) {
    if (file.type !== 'image/png' && file.type !== 'image/jpeg') return;
    if (file.size > 5_000_000) return;

    file.arrayBuffer().then((data) => {
        $('.product-image').attr('src', URL.createObjectURL(new Blob([data])));
        $('.product-image-action').removeClass('hidden');
    });
    curfile = file;
}

function loadCategories(cur_category) {
    let category = $('#category');
    $.ajax({
        method: 'GET',
        url: '/backend/api/product/categories',
        contentType: 'application/json',
        success: (resp) => {
            resp.data.forEach((data) => {
                let option = $('<option></option>').html(data.name).val(data.id);
                category.append(option);
            });
            let option = $('<option></option>').html('Uncategorized').val(-1);
            category.append(option);
            category.val(cur_category);
        },
        error: (e) => {
            console.log(e);
        }
    });
}

function setContent(data) {
    let cur_category = -1;
    if (data.category !== null) cur_category = data.category.id;
    loadCategories(cur_category);

    $('#name').val(data.name);
    $('#price').val(data.price);
    $('#description').val(data.description);
    $('#recommended').prop('checked', data.recommended);
    $('#trait-savory').prop('checked', data.traits.includes('SAVORY'));
    $('#trait-sweet').prop('checked', data.traits.includes('SWEET'));
    $('#trait-spicy').prop('checked', data.traits.includes('SPICY'));
    if (!data.imageUrl) {
        $('.product-image').attr('src', '/img/placeholder.png');
    } else {
        $.ajax({
            type: 'GET',
            url: '/backend' + data.imageUrl,
            xhr: () => {
                let xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                return xhr;
            },
            success: (response) => {
                $('.product-image').attr('src', URL.createObjectURL(response));
                $('.product-image-action').removeClass('hidden');
            },
            error: function (e) {
                console.log(e);
            }
        });
    }
}