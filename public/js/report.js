const DAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

let dateSelector;

function onContentLoad() {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    today.setDate(today.getDate() - 7)

    dateSelector = $('#generate-report-date');
    for (let i = 0; i < 7; i++) {
        today.setDate(today.getDate() + 1);
        dateSelector.append($('<option></option>').html(DAYS[today.getDay()] + ', ' + today.getDate() + ' ' + MONTHS[today.getMonth()] + ' ' + today.getFullYear()).val(today.getTime()));
    }
    dateSelector.val(today.getTime());

    $('#generate-report').click(() => {
        fetchOrders();
    });

    $('.overlay').click(() => {
        $('.order-detail').fadeOut(150);
    });

    fetchOrders();
}

function fetchOrders() {

    $.ajax({
        method: 'GET',
        url: `/backend/api/admin/report/reports?timestamp=${dateSelector.val()}`,
        dataType: 'json',
        contentType: 'application/json',
        success: (resp) => {
            $('.order:not(#order-template)').remove();
            loadOrders(resp);
        },
        error: (e) => {
            console.log(e);
        }
    });
}

function viewDetail(data) {
    let detail = $('.order-detail');

    let date = new Date(data.timestamp);
    let dateAfter = new Date(data.timestamp);
    dateAfter.setTime(dateAfter.getTime() + 2 * 60 * 60 * 1000);
    detail.find('.order-detail-date').html(`${DAYS[date.getDay()]}, ${date.getDate()} ${MONTHS[date.getMonth()]} ${date.getFullYear()}`);
    detail.find('.order-detail-time').html(`${String(date.getHours()).padStart(2, '0')}:${String(dateAfter.getMinutes()).padStart(2, '0')} - ${String(dateAfter.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`);

    detail.find('.order-detail-tablenum').html(data.tableId);

    detail.find('.reserver-name').html(data.fullName);
    detail.find('.reserver-email').html(data.email);

    let totalprice = 0;

    let template = $('#orderedproduct-template');
    let orderedproducts = detail.find('.modal-content');
    $('.orderedproduct:not(#orderedproduct-template)').remove();
    data.orders.forEach((orderdata) => {
        let orderedproduct = template.clone().removeAttr('id');
        orderedproduct.find('.orderedproduct-amount').html(orderdata.quantity);
        orderedproduct.find('.orderedproduct-name').html(orderdata.product.name);
        totalprice += orderdata.quantity * orderdata.product.price;

        if (!orderdata.served) {
            orderedproduct.find('.orderedproduct-done').remove();
        }

        if (orderdata.product.imageUrl != null) {
            $.ajax({
                type: 'GET',
                url: '/backend' + orderdata.product.imageUrl,
                xhr: () => {
                    let xhr = new XMLHttpRequest();
                    xhr.responseType = 'blob';
                    return xhr;
                },
                success: (response) => {
                    orderedproduct.find('.orderedproduct-image').attr('src', URL.createObjectURL(response));
                },
                error: function(e) {
                    console.log(e);
                }
            });
        }

        orderedproducts.append(orderedproduct);
    });

    detail.find('.reservation-total').html('Rp. ' + totalprice);

    detail.fadeIn(150);
}

function loadOrders(resp) {
    let orders = $('.orders');
    let template = $('#order-template');
    resp.data.reservations.forEach(data => {
        let order = template.clone().removeAttr('id');
        order.find('.order-id').html(data.id);

        let date = new Date(data.timestamp);
        let dateAfter = new Date(data.timestamp);
        dateAfter.setTime(dateAfter.getTime() + 2 * 60 * 60 * 1000);
        order.find('.date').html(`${DAYS[date.getDay()]}, ${date.getDate()} ${MONTHS[date.getMonth()]} ${date.getFullYear()}`);
        order.find('.time').html(`${String(date.getHours()).padStart(2, '0')}:${String(dateAfter.getMinutes()).padStart(2, '0')} - ${String(dateAfter.getHours()).padStart(2, '0')}:${String(date.getMinutes()).padStart(2, '0')}`);

        order.find('.order-table-number-no').html(data.tableId);

        order.find('.order-detail-btn a').click(() => {
            viewDetail(data);
        });

        orders.append(order);
    });
    $('.report-total span').html(resp.data.total);
}