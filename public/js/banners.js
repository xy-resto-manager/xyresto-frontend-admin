function onContentLoad() {
    $.ajax({
        method: 'GET',
        url: '/backend/api/banner/banners',
        dataType: 'json',
        contentType: 'application/json',
        success: (resp) => {
            loadBanners(resp);
        },
        error: (e) => {
            console.log(e);
        }
    });

    $('#create-banner').click(() => {
        $('<input>').attr('type', 'file').change(function() {
            let file = $(this)[0].files[0];
            createBannerImage(file);
        }).click();
    });
}

function createBannerImage(file) {
    if (file.type !== 'image/png' && file.type !== 'image/jpeg') return;
    if (file.size > 5_000_000) return;

    let formdata = new FormData();
    file.arrayBuffer().then((data) => {
        formdata.append('file', file);
        $.ajax({
            method: 'POST',
            url: '/backend/api/admin/banner/banners',
            dataType: 'json',
            data: formdata,
            contentType: false,
            processData: false,
            success: (resp) => {
                let url = URL.createObjectURL(new Blob([data]));
                let banner = $('#banner-template').clone().removeAttr('id');

                banner.find('img').attr('src', url);
                banner.attr('data-id', resp.data.id);
                banner.click(() => {
                    bannerClick(banner);
                });
                $('#create-banner').before(banner);
                bannerClick(banner);
            },
            error: (e) => {
                console.log(e);
            }
        });
    });
}

function loadBanners(resp) {
    let template = $('#banner-template')
    let create = $('#create-banner');
    let first = true;
    resp.data.forEach((data) => {
        let banner = template.clone().removeAttr('id');
        let setclick = first;
        if (first) {
            first = false;
        }
        $.ajax({
            type: 'GET',
            url: '/backend' + data.imageUrl,
            xhr: () => {
                let xhr = new XMLHttpRequest();
                xhr.responseType = 'blob';
                return xhr;
            },
            success: (response) => {
                banner.find('img').attr('src', URL.createObjectURL(response));
                if (setclick) {
                    bannerClick(banner);
                }
            },
            error: function (e) {
                console.log(e);
            }
        });
        banner.attr('data-id', data.id);
        banner.click(() => {
            bannerClick(banner);
        });
        create.before(banner);
    });
}

function bannerClick(e) {
    $('.selected').removeClass('selected');
    e.addClass('selected');
    $('#banner-preview').attr('src', e.find('img').attr('src'));
    $('#delete-banner').off('click');
    $('#delete-banner').click(() => {
        deleteBanner(e);
    });
}

function deleteBanner(e) {
    let id = e.attr('data-id');
    $.ajax({
        method: 'DELETE',
        url: `/backend/api/admin/banner/${id}`,
        dataType: 'json',
        contentType: 'application/json',
        success: () => {
            e.remove();
            let next = $('.banner:not(#create-banner):eq(1)');
            if (next.length) {
                bannerClick(next);
            } else {
                $('#banner-preview').attr('src', '/img/banner-placeholder.jpg');
            }
        },
        error: (e) => {
            console.log(e);
        }
    })
}