var page = 1;
var category = 1;

function onContentLoad() {
    $('.alert-close').click(() => {
        $('.alert').css('transform', 'translateY(-10rem)');
        window.history.replaceState({}, document.title, window.location.pathname);
    });

    if (window.location.href.includes('c=1')) {
        $('.alert-created').css('transform', 'translateY(-10rem)').show().css('transform', 'translateY(0)');
        $('.alert:not(.alert-created)').remove();
    } else if (window.location.href.includes('e=1')) {
        $('.alert-edited').css('transform', 'translateY(-10rem)').show().css('transform', 'translateY(0)');
        $('.alert:not(.alert-edited)').remove();
    }

    $.ajax({
        type: 'GET',
        url: '/backend/api/product/categories',
        success: loadCategories,
        error: (e) => {
            console.log(e);
        }
    });

    $('.edit-category').click(() => {
        $('.category-action-edit').toggle();
        $('.category-new').toggle();
    });

    $('.category-action-create').click(() => {
        let data = {
            name: $('.category-new input').val()
        }
        $.ajax({
            method: 'POST',
            url: '/backend/api/admin/product/categories',
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: 'application/json',
            success: (resp) => {
                let category = $('#category-template').clone().removeAttr('id');
                category.attr('data-id', resp.data.id);
                category.find('span').html(resp.data.name);
                category.find('.category-action-edit').show();
                category.click(() => {
                    changeCategory(category, resp.data.id, resp.data.name);
                });
                category.find('div').click(() => {
                    deleteCategory(category, resp.data.id);
                });
                $('.category-new').before(category);
                $('.category-new input').val('');
            },
            error: (e) => {
                console.log(e);
            }
        })
    });

    addScrollHandling();
}

function addScrollHandling() {
    $('.products').scroll(function() {
        let scrollpos = $(this)[0].scrollHeight - $(this).height() - $(this).scrollTop();

        if (scrollpos < 1) {
            loadMoreProducts();
        }
    });
}

function loadCategories(resp) {
    let template = $('#category-template');
    let first = true;
    resp.data.forEach((data) => {
        let category = template.clone().removeAttr('id');
        category.attr('data-id', data.id);
        category.find('span').html(data.name);
        category.find('.category-action-edit').hide();
        category.click(() => {
            changeCategory(category, data.id, data.name);
        });
        category.find('div').click(() => {
            deleteCategory(category, data.id);
        });
        $('.category-new').before(category);
        if (first) {
            changeCategory(category, data.id, data.name);
            first = false;
        }
    });
    let category = template.clone().removeAttr('id');
    category.attr('data-id', -1);
    category.find('span').html('Uncategorized');
    category.find('.category-action-edit').remove();
    category.click(() => {
        changeCategory(category, -1, 'Uncategorized');
    });
    $('.category-new').before(category);
    if (first) {
        changeCategory(category, -1, 'Uncategorized');
    }
}

function deleteCategory(element, id) {
    $.ajax({
        type: 'DELETE',
        url: '/backend/api/admin/product/categories/' + id,
        contentType: 'application/json',
        success: () => {
            element.remove();
            if (category === id) {
                let categoryElement = $('.category:not(#category-template)').first();
                changeCategory(categoryElement, parseInt(categoryElement.attr('data-id')), categoryElement.find('span').html());
            }
        },
        error: (e) => {
            console.log(e);
        }
    });
}

function changeCategory(element, id, name) {
    category = id;
    page = 1;
    $('.category').removeClass('active');
    element.addClass('active');

    $('#main-content-title').html(name);

    $.ajax({
        type: 'GET',
        url: '/backend/api/product/products?page=1&sort=' + (id === -1 ? 'null' : id),
        success: (resp) => {
            $('.product:not(#product-template)').remove();
            addScrollHandling();
            loadProducts(resp);

            let products = $('.products');
            console.log(products[0].scrollHeight < $('.main-content').height() - $('.main-content-header').height());
            while (products[0].scrollHeight < $('.main-content').height() - $('.main-content-header').height()) {
                if (resp.pagination.totalItems <= resp.pagination.itemsPerPage * resp.pagination.currentPage) return;
                loadMoreProducts();
            }
        },
        error: (e) => {
            console.log(e);
        }
    });
}

function loadProducts(resp) {
    let template = $('#product-template');
    let products = $('.products');

    if (resp.data.length === 0) {
        $('.products').off('scroll');
    }

    resp.data.forEach((data) => {
        let product = template.clone().removeAttr('id');
        product.find('h3').html(data.name);

        if (data.imageUrl != null) {
            $.ajax({
                type: 'GET',
                url: '/backend' + data.imageUrl,
                xhr: () => {
                    let xhr = new XMLHttpRequest();
                    xhr.responseType = 'blob';
                    return xhr;
                },
                success: (response) => {
                    product.find('img').attr('src', URL.createObjectURL(response));
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        product.find('.product-action-edit').click(() => {
            window.location.href = '/edit-product/' + data.id;
        });
        product.find('.product-action-delete').click(() => {
            $.ajax({
                type: 'DELETE',
                url: '/backend/api/admin/product/' + data.id,
                success: () => {
                    product.remove();
                },
                error: (e) => {
                    console.log(e);
                }
            });
        });
        products.append(product);
    });
}

function loadMoreProducts() {
    page++;
    $.ajax({
        async: false,
        type: 'GET',
        url: '/backend/api/product/products?page=' + page + '&sort=' + category,
        success: (resp) => {
            loadProducts(resp);
        },
        error: (e) => {
            console.log(e);
        }
    });
}
