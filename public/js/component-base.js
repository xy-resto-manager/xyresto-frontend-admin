$('.component-base').load('/component/base.html', null, function() {
    $(this).replaceWith($(this).html());

    $('.header-title').html(header_title);
    $(`.navbar-menus .navbar-nav:nth-child(${navbar_active})`).addClass('active');

    $('.header-menu').click((e) => {
        openMenu(e);
    });
    $('.navbar').click((e) => {
        e.stopPropagation();
    });
    $('.navbar-close').click(() => {
        closeMenu();
    });
    $('.navbar-end').click(()=>{
        logout();
    })
});

let menu_opened = false;

function openMenu(e) {
    if (menu_opened) return;
    e.stopPropagation();

    let navbar = $('.navbar');
    navbar.css('transform', 'translateX(0)');
    $(window).click(() => {
        closeMenu();
    });

    menu_opened = true;
}

function closeMenu() {
    if (!menu_opened) return;

    $(window).off('click');

    let navbar = $('.navbar');
    navbar.css('transform', 'translateX(-100%)');

    menu_opened = false;
}

function logout(){
    $.ajax({
        type: 'POST',
        url: '/backend/api/user/_logout',
        success: (resp) => {
            window.location.href = '/';
        },
        error: (e) => {
            console.log(e);
        }
    });
}