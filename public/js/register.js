$.ajax({
    type: 'GET',
    url: '/backend/api/user/current-user',
    success: () => { window.location.href = '/' }
});

function onContentLoad() {
    $('#register').click(tryRegister);
    $(document).keypress((e) => {
        if (e.which == 13) tryRegister();
    });
}

function tryRegister() {
    if (!$('.register-form')[0].checkValidity()) {
        $('<input type="submit">').hide().appendTo($('.register-form')).click().remove();
        return;
    }
    if ($('#password').val() !== $('#confirmpassword').val()) {
        $('.errorConfirm').fadeIn(150);
    }
    let postData = {
        email: $('#email').val(),
        password: $('#password').val(),
        fullName: $('#name').val()
    }
    $.ajax({
        type: 'POST',
        url: '/backend/api/user/_register',
        data: JSON.stringify(postData),
        contentType: 'application/json',
        dataType: 'json',
        success: registerSuccess,
        error: registerFailed
    });
}

function registerSuccess(response) {
    window.location.href = '/login?reg=1';
}

function registerFailed(e) {
    $('.error').fadeIn(150);
}