let currentDate = null;
let currentTime = "09:00:00"; // Default Value;
let canvas = null;
let ctx = null;
let width = null;
let height = null;
let selectedTable = null;
let selectedTableX = null;
let selectedTableY = null;
let tableData = null;
let gridWidth = null;
let gridHeight = null;
let tileWidth = null;
let tileHeight = null;
let timestamp = null; 
let cartData = null; 

function onContentLoad(){

    canvas = document.querySelector('.canvas');
    ctx = canvas.getContext('2d');
    width = Math.round(canvas.offsetWidth);
    height = Math.round(canvas.offsetHeight);
    ctx.canvas.width = width;
    ctx.canvas.height = height;
    gridWidth = width / 5;
    gridHeight = height / 5;
    tileWidth = gridWidth * 0.8;
    tileHeight = gridHeight * 0.8;
    
    $('#input-date').change(()=>{
        currentDate = $('#input-date').val()
        getReseravtionOnTimeStamp();
    })
    $('#input-time').change(()=>{
        currentTime = $('#input-time').val()
        getReseravtionOnTimeStamp();
    })

    $('.canvas').click((e)=>{
        checkIfTableIsAvailable(e);
    })

    $('.reserve-btn').click(() => {
        if(selectedTable != null){
            timestamp = new Date(currentDate + "T" + currentTime+"Z").getTime() - 7 * 3600000;
            console.log(timestamp)
            let postData = {    
                timestamp : timestamp,
                tableId : selectedTable
            }
            $.ajax({
                type: 'POST',
                url: '/backend/api/reservation/reservations',
                data: JSON.stringify(postData),
                contentType: 'application/json',
                dataType: 'json',
                success: reservationSuccess,
                error: function(e){
                    console.log(e)
                    if(e.responseJSON.errors[0].error == "ExpiredDateException"){
                        showModal('Expired')
                    }else if(e.responseJSON.errors[0].error == "IsBookedException"){
                        showModal("Booked")
                    }else{
                        console.log(e)
                    }
                }
            });
        }
    })

    

}

function reservationSuccess(){
    window.location.href = '/reservation';
}

function reservationFailed(){
    console.log("failed")
}

function getReseravtionOnTimeStamp(){
    if(currentDate != null && currentTime != null){
        ctx.clearRect(0,0,width,height);
        // Ini masih local time, mau disimpen di database UTC / GMT jadi dikarenakan timezone kita GMT+7 dikurangi 7 jam 
        timestamp = new Date(currentDate + "T" + currentTime+"Z").getTime() - 7 * 3600000;
        $.ajax({
            type: 'GET',
            url: '/backend/api/layout/tables/?timestamp='+timestamp,
            dataType : 'json',
            success : initLayout,
            error: function(e){
                console.log(e)
        }
        });
    }
}

function initLayout(response){
    console.log(response.data)
    tableData = response.data

    response.data.forEach(table => {
        var startXDrawPos = (table.table.x - 1) * gridWidth + 0.1 * gridWidth;
        var startYDrawPos = (table.table.y - 1) * gridHeight + 0.1 * gridHeight;
        var tableNo = table.table.tableId
         ctx.beginPath();
         ctx.font = '24px serif';
         if(table.booked == true){
           drawBooked(startXDrawPos,startYDrawPos,tableNo)
         }else{
            drawAvailable(startXDrawPos,startYDrawPos,tableNo)
         }
         
    });

    tableData = response.data;
}

function checkIfTableIsAvailable(e){

    let xPos = Math.ceil(e.offsetX / gridWidth)
    let yPos = Math.ceil(e.offsetY / gridHeight)

    tableData.forEach(table => {

        var startXDrawPos = (table.table.x - 1) * gridWidth + 0.1 * gridWidth;
        var startYDrawPos = (table.table.y - 1) * gridHeight + 0.1 * gridHeight;
         if(table.table.x == xPos && table.table.y == yPos){
             if(table.booked == true){
                showModal('Booked')
             }else{
                 if(selectedTable == null){
                     selectedTable = table.table.tableId;
                     selectedTableX = table.table.x;
                     selectedTableY = table.table.y;
                     drawSelected(startXDrawPos,startYDrawPos,selectedTable)
                     
                 }else{
                     if(selectedTable == table.table.tableId){
                        drawAvailable(startXDrawPos,startYDrawPos,selectedTable);
                        selectedTable = null;
                        selectedTableX = null;
                        selectedTableY = null;
                     }else{
                        var resetRectStartXDrawPos = (selectedTableX - 1) * gridWidth + 0.1 * gridWidth;
                        var resetRectStartYDrawPos = (selectedTableY - 1) * gridHeight + 0.1 * gridHeight;
                        drawAvailable(resetRectStartXDrawPos,resetRectStartYDrawPos,selectedTable);
                        // 
                        selectedTable = table.table.tableId;
                        selectedTableX = table.table.x;
                        selectedTableY = table.table.y;
                        drawSelected(startXDrawPos,startYDrawPos,selectedTable)
                     }
                 }
             }
         }
    });
    $('.table-no > span').text(selectedTable)
    $('.capacity > span').text(4)
}

function showModal(string){
    if(string == "Booked"){
        $('.box > p').text('Table is Booked!')
    }else if(string == "Expired"){
        $('.box > p').text("Expired Date!")
    }
    $('.overlay').css({display: 'flex',"z-index" : 3}).animate({
        opacity: 1
    }, 150);
    $('.overlay').show()
    $('.ok-btn').click(()=>{
        $('.overlay').hide()
    })
}

function drawBooked(startXDrawPos,startYDrawPos,tableNo){
    ctx.beginPath();
    ctx.fillStyle = "#F9F7F0";
            ctx.fillRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
            ctx.fillStyle = "#242424";
            ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight)
            ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight))
}

function drawAvailable(startXDrawPos,startYDrawPos,tableNo){
    ctx.beginPath();
    ctx.clearRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.strokeRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#242424"
    ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight))
}

function drawSelected(startXDrawPos,startYDrawPos,tableNo){
    
    ctx.beginPath();
    ctx.clearRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight)
    ctx.fillStyle = "#E4C102";
    ctx.fillRect(startXDrawPos,startYDrawPos,tileWidth,tileHeight);
    ctx.fillStyle = "#F9F7F0";
    ctx.fillText(tableNo,(startXDrawPos + 0.35 * gridWidth),(startYDrawPos + 0.5 * gridHeight))

}